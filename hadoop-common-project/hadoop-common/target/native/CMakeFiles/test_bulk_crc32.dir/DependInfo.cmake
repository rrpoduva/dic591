# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/src/main/native/src/org/apache/hadoop/util/bulk_crc32.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/target/native/CMakeFiles/test_bulk_crc32.dir/main/native/src/org/apache/hadoop/util/bulk_crc32.c.o"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/src/main/native/src/test/org/apache/hadoop/util/test_bulk_crc32.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/target/native/CMakeFiles/test_bulk_crc32.dir/main/native/src/test/org/apache/hadoop/util/test_bulk_crc32.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "javah"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/src/main/native/src"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/src"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/src/src"
  "."
  "/home/hadoopuser/java/jdk1.8.0_40/include"
  "/home/hadoopuser/java/jdk1.8.0_40/include/linux"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-common-project/hadoop-common/src/main/native/src/org/apache/hadoop/util"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
