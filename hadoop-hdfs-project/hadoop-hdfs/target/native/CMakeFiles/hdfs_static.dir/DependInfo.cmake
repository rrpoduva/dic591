# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs/common/htable.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/target/native/CMakeFiles/hdfs_static.dir/main/native/libhdfs/common/htable.c.o"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs/exception.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/target/native/CMakeFiles/hdfs_static.dir/main/native/libhdfs/exception.c.o"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs/hdfs.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/target/native/CMakeFiles/hdfs_static.dir/main/native/libhdfs/hdfs.c.o"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs/jni_helper.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/target/native/CMakeFiles/hdfs_static.dir/main/native/libhdfs/jni_helper.c.o"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs/os/posix/mutexes.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/target/native/CMakeFiles/hdfs_static.dir/main/native/libhdfs/os/posix/mutexes.c.o"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs/os/posix/thread_local_storage.c" "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/target/native/CMakeFiles/hdfs_static.dir/main/native/libhdfs/os/posix/thread_local_storage.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "javah"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src"
  "."
  "/home/hadoopuser/java/jdk1.8.0_40/include"
  "/home/hadoopuser/java/jdk1.8.0_40/include/linux"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs"
  "/home/hadoopuser/hadoop-2.6.0-src/hadoop-hdfs-project/hadoop-hdfs/src/main/native/libhdfs/os/posix"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
